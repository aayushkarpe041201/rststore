import { Grid, Heading,Flex } from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { listProducts } from "../actions/productActions";
import Loader from "../components/Loader";
import Message from "../components/Message";
import ProductCard from "../components/ProductCard";

const HomeScreen = () => {
  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;

  useEffect(() => {
    dispatch(listProducts());
  }, [dispatch]);

  return (
    <>
      <Heading as="h2" mb="8" fontSize="xl">
        Latest products
      </Heading>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (

          <Flex justifyContent={'space-around'} flexFlow={'row wrap'} >
              {products.map((product) => (
            <ProductCard key={product._id} product={product} />
          ))}
          </Flex>

        // <Grid templateColumns="1fr 1fr 1fr 1fr" gap="8">
        //   {products.map((product) => (
        //     <ProductCard key={product._id} product={product} />
        //   ))}
        // </Grid>
      )}
    </>
  );
};

export default HomeScreen;
